package com.hit.memory;
import com.hit.algorithm.IAlgoCache;
import com.hit.dm.DataModel;

@SuppressWarnings("unchecked")
public class CacheUnit<T> {
	
	private IAlgoCache<Long, DataModel<T>> algo;

	public CacheUnit(IAlgoCache<Long,DataModel<T>> algo) {
		this.algo = algo;			
	}
	
	public DataModel<T>[] getDataModels(Long[] ids) {
		DataModel<T>[] result = new DataModel[ids.length];

		for (int i = 0; i < ids.length; i++) {
			DataModel<T> e = this.algo.getElement(ids[i]);
			if(e == null)
				continue;
			result[i] = e;
		}
		
		return result;
	}
	
	
	public DataModel<T>[] putDataModels(DataModel<T>[] datamodels) {
		DataModel<T>[] result = new DataModel[datamodels.length];
		
		for (int i = 0; i < datamodels.length; i++) {
			DataModel<T> model = datamodels[i];
			Long modelID = model.getDataModelId();
			DataModel<T> e = this.algo.putElement(modelID, model);
			if(e == null)
				continue;
			result[i] = e;
		}
		
		return result;
		
	}
	
	public void	removeDataModels(Long[] ids) {
		for (Long id : ids) {
			this.algo.removeElement(id);
		}
	}
}
