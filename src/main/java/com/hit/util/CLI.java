package com.hit.util;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Scanner;


public class CLI implements Runnable {

	private InputStream in;
	private OutputStream out;
	private PropertyChangeSupport listeners;
	private String action;

	public CLI(InputStream in, OutputStream out) {
		this.in = in;
		this.out = out;
		this.listeners = new PropertyChangeSupport(this);
		action = "";
	}

	public void addPropertyChangeListener(PropertyChangeListener pcl){
		this.listeners.addPropertyChangeListener(pcl);
	}
	
	public void removePropertyChangeListener(PropertyChangeListener pcl) {
		this.listeners.removePropertyChangeListener(pcl);
	}
	
	public void write(String string) {
		
	}

	@Override
	public void run() {
		try(PrintStream ps = new PrintStream(this.out)){
				try(Scanner scan = new Scanner(this.in)){
					while(true) {
						ps.println("Please enter your command");
						String newAction = scan.nextLine();
						
						if(newAction.equalsIgnoreCase("start")) {
							ps.println("Starting server.......");
							this.listeners.firePropertyChange("action", this.action, newAction);
							this.action = newAction;
						}else if(newAction.equalsIgnoreCase("stop")) {
							ps.println("Shutdown server"); 
							this.listeners.firePropertyChange("action", this.action, newAction);
							this.action = newAction;
						}
						else {
							ps.println("Not a valid command");
						}
						
					}
				}
		}
		
	}

}
