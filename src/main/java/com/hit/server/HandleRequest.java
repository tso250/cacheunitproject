package com.hit.server;


import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.net.Socket;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hit.dm.DataModel;
import com.hit.dm.Response;
import com.hit.dm.StatsModel;
import com.hit.services.CacheUnitController;

public class HandleRequest<T> implements Runnable{
	private static Gson gsonParser = new Gson();
	private Socket clientSocket;
	private CacheUnitController<T> cacheController;
	
	public HandleRequest(Socket s, CacheUnitController<T> controller){
		this.clientSocket = s;
		this.cacheController = controller;
	}

	@Override
	public void run() {
		try(Scanner reader = new Scanner(new InputStreamReader(this.clientSocket.getInputStream()))){
			try(PrintWriter writer = new PrintWriter(new OutputStreamWriter(this.clientSocket.getOutputStream()))){
				Type ref = new TypeToken<Request<DataModel<T>[]>>(){}.getType(); 
				String jsonRequest = reader.nextLine();
				Request<DataModel<T>[]> request = HandleRequest.gsonParser.fromJson(jsonRequest, ref); 
				String action = request.getHeaders().get("action");
				
				Response<?> response = new Response<String>(action, false);
				DataModel<T>[] clientData = request.getBody();
				switch(action) {
					case "UPDATE":
						if(this.cacheController.update(clientData))
							response = new Response<String>(action, true);
						break;
					case "GET":
						DataModel<T>[] result = this.cacheController.get(clientData);
						if(result != null)
							response = new Response<DataModel<T>[]>(action, result);
						break;
					case "DELETE":
						if(this.cacheController.delete(clientData))
							response = new Response<String>(action, true);
						break;
					case "GETSTATS":
						response = new Response<StatsModel>(action, this.cacheController.getStats());
						break;
						
				}
				
				writer.println(HandleRequest.gsonParser.toJson(response));
				writer.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if(this.clientSocket != null && this.clientSocket.isConnected())
				try {
					this.clientSocket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		
	}
	
}
