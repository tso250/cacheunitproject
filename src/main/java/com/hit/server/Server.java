package com.hit.server;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import com.hit.services.CacheUnitController;

public class Server implements PropertyChangeListener, Runnable{

	
	private ServerSocket serverSocket;
	private boolean isAlive;
	private Thread serverThread;
	private CacheUnitController<String> cacheUnitController;
	private Executor threadPool;

	public Server() {
			this.cacheUnitController =  new CacheUnitController<String>();
			this.isAlive = false;
			this.threadPool = Executors.newCachedThreadPool();
	}
	

	@Override
	public void run() {
		
		if(!this.isAlive)
			return;
		
		try(ServerSocket serverSocket = new ServerSocket(12345)){
			this.serverSocket = serverSocket;
			while(this.isAlive) {
				try {
					Socket clientSocket = this.serverSocket.accept();
					HandleRequest<String> request = new HandleRequest<String>(clientSocket, cacheUnitController);
					this.threadPool.execute(request);
				}
				catch(SocketException e) {
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		this.serverSocket = null;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if(!evt.getPropertyName().equals("action"))
			return;
		String action = (String) evt.getNewValue();
		if(action.equalsIgnoreCase("start")) {
			this.isAlive = true;
			this.serverThread = new Thread(this);
			this.serverThread.start();
		}
		else if(action.equalsIgnoreCase("stop")) {
			try {
				this.isAlive = false;
				if(this.serverThread != null)
					this.serverThread.join(2000);
				if(this.serverSocket != null && !this.serverSocket.isClosed()) // should we close the server socket?
					this.serverSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
}
