package com.hit.dm;

public class StatsModel {

	private String algoName;
	private Integer capacity;
	private Integer totalRequests;
	private Integer totalDataModels;
	private Integer totalDataModelsSwaps;
	
	public StatsModel() {
		this.algoName = "";
		this.capacity = 0;
		this.totalDataModels = 0;
		this.totalRequests = 0;
		this.totalDataModelsSwaps = 0;
	}
	
	public String getAlgoName() {
		return algoName;
	}
	public void setAlgoName(String algoName) {
		this.algoName = algoName;
	}
	public Integer getCapacity() {
		return this.capacity;
	}
	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}
	public Integer getTotalRequests() {
		return totalRequests;
	}
	public void setTotalRequests(Integer totalRequests) {
		this.totalRequests = totalRequests;
	}
	public Integer getTotalDataModels() {
		return totalDataModels;
	}
	public void setTotalDataModels(Integer totalDataModels) {
		this.totalDataModels = totalDataModels;
	}
	public Integer getTotalDataModelsSwaps() {
		return totalDataModelsSwaps;
	}
	public void setTotalDataModelsSwaps(Integer totalDataModelsSwaps) {
		this.totalDataModelsSwaps = totalDataModelsSwaps;
	}
	public void increaseSwaps() {
		this.totalDataModelsSwaps++;
	}
	
	public void increaseRequests() {
		this.totalRequests++;
	}
	
	public void increaseTotalDataModelsRequests() {
		this.totalDataModels++;
	}
	
	public void increaseTotalDataModelsRequests(int length) {
		this.totalDataModels += length;
	}

}
