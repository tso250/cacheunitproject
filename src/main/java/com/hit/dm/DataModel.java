package com.hit.dm;

import java.io.Serializable;

@SuppressWarnings("unchecked")
public class DataModel<T> implements Serializable{
	

	private static final long serialVersionUID = 1L;
	private Long id;
	private T content;

	public DataModel(Long id, T content) {
		this.id = id;
		this.content = content;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj == null)
			return false;
		
		if(!(obj instanceof DataModel))
			return false;
		
		if(obj == this)
			return true;
		
		DataModel<T> other = (DataModel<T>)obj;
		
		return this.id.equals(other.id);
	}
	
	public T getContent() {
		return this.content;
	}
	
	public Long getDataModelId(){
		return this.id;
	}
	
	@Override
	public int hashCode() {
		return this.id.hashCode();
	}
	
	public void setContent(T content) {
		this.content = content;
	}
	
	public void setDataModelId(Long id) {
		this.id = id;
	}
	
	@Override
	public String toString()
	{
		return "id[" + this.id + "] " + "content[" + this.content + "]";
	}
	
}
