package com.hit.dm;

public class Response<T> {
	T data;
	String type;
	boolean successed;
	

	public Response(String type, boolean successed) {
		this.type = type;
		this.successed = successed;
	}
	
	public Response(String type, T data) {
		this.type = type;
		this.data = data;
		this.successed = true;
	}
	
	public boolean isSuccessed() {
		return successed;
	}

	public void setSuccessed(boolean successed) {
		this.successed = successed;
	}
	
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

}
