package com.hit.dao;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import com.hit.dm.DataModel;

@SuppressWarnings("unchecked")
public class DaoFileImpl<T> implements IDao<Long, DataModel<T>>{
	private String filePath;
	private int capacity;

	public DaoFileImpl(String filePath) {
		this.filePath = filePath;
		this.capacity = Integer.MAX_VALUE;
	}
	
	public DaoFileImpl(String filePath, int capacity) {
		this.filePath = filePath;
		this.capacity = capacity;
	}

	public int getCapacity() {
		return capacity;
	}

	@Override
	public void delete(DataModel<T> entity) throws IllegalArgumentException, IOException, ClassNotFoundException{
		HashMap<Long, DataModel<T>> hashed;

		if(entity == null)
			throw new IllegalArgumentException("entity");
		hashed = readFromFile();
		
		if(hashed == null)
			return;
		
		if(hashed.containsKey(entity.getDataModelId())) {
			hashed.remove(entity.getDataModelId());
			saveToFile(hashed);
			this.capacity++;
		}

	}

	@Override
	public DataModel<T> find(Long id) throws ClassNotFoundException, IOException {
		if(id == null)
			throw new IllegalArgumentException("id");
		HashMap<Long, DataModel<T>> hashed;
		hashed = readFromFile();
		if(hashed == null)
			return null;
		if(hashed.containsKey(id)) {
				return hashed.get(id);
		}
		return null;
	}

	@Override
	public void save(DataModel<T> entity) throws IOException, ClassNotFoundException {

		if(entity == null || entity.getDataModelId() == null)
			return;
		
		if(this.capacity == 0) 
			return;
		
		HashMap<Long, DataModel<T>> hashed = readFromFile();
		
		if(hashed == null){
			// no data in file
			hashed = new HashMap<Long, DataModel<T>>();
		}
		
		if(!hashed.containsKey(entity.getDataModelId())) {
			this.capacity--;
		}
		hashed.put(entity.getDataModelId(), entity);
		
		saveToFile(hashed);

	}
	
	private void saveToFile(HashMap<Long, DataModel<T>> hashed) throws IOException {
		
		try(FileOutputStream fout = new FileOutputStream(this.filePath)){
			try(BufferedOutputStream bos = new BufferedOutputStream(fout)){
				try(ObjectOutputStream oos = new ObjectOutputStream(bos)){
					oos.writeObject(hashed);
				}
			}
		}
	}
	
	private HashMap<Long, DataModel<T>> readFromFile() throws IOException, ClassNotFoundException{
		HashMap<Long, DataModel<T>> hashed;
		try(FileInputStream fin = new FileInputStream(this.filePath)){
			try(BufferedInputStream bis = new BufferedInputStream(fin)){
				try(ObjectInputStream ois = new ObjectInputStream(bis)){
					hashed = (HashMap<Long, DataModel<T>>) ois.readObject();
				}
			}
		}
		catch(IOException | ClassNotFoundException e) {
			e.printStackTrace();
			throw e;
		}
		
		return hashed;
	}

}
