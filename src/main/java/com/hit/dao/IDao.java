package com.hit.dao;

import java.io.IOException;
import java.io.Serializable;

public interface IDao<ID extends Serializable, T> {
	void delete(T entity) throws IllegalArgumentException, IOException, ClassNotFoundException;
	T  find(ID id) throws IllegalArgumentException, IOException, ClassNotFoundException;
	void save(T entity) throws IOException, ClassNotFoundException;
}
