package com.hit.services;

import java.io.File;
import java.io.IOException;
import com.hit.algorithm.IAlgoCache;
import com.hit.algorithm.LRUAlgoCacheImpl;
import com.hit.algorithm.NRUAlgoCacheImpl;
import com.hit.dao.DaoFileImpl;
import com.hit.dm.DataModel;
import com.hit.dm.StatsModel;
import com.hit.memory.CacheUnit;

@SuppressWarnings("unchecked")
public class CacheUnitService<T> {
	private CacheUnit<T> cacheUnit;
	private DaoFileImpl<T> dao;
	private StatsModel statsModel;
	private int cacheCapacity;
	private String algoName;

	public CacheUnitService() {
		this.statsModel = new StatsModel();
		this.cacheCapacity = 1;
		this.algoName = "LRU";
		IAlgoCache<Long, DataModel<T>> defaultAlgo = new LRUAlgoCacheImpl<Long, DataModel<T>>(this.cacheCapacity);
		this.statsModel.setAlgoName(this.algoName);
		this.statsModel.setCapacity(this.cacheCapacity);
		this.cacheUnit = new CacheUnit<T>(defaultAlgo);
		this.dao = new DaoFileImpl<T>(new File("src/main/resources/datasource.txt").getAbsolutePath());
	}
	
	public boolean update(DataModel<T>[] dataModels) {
		this.statsModel.increaseRequests();
		this.statsModel.increaseTotalDataModelsRequests(dataModels.length);
		DataModel<T>[] removed = this.cacheUnit.putDataModels(dataModels);
		boolean savedSuccessfully = true;
		for (DataModel<T> dataModel : removed) {
			if(dataModel == null)
				continue;
			this.statsModel.increaseSwaps();
			try {
				this.dao.save(dataModel);
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
				savedSuccessfully = false;
			}
		}
		
		return savedSuccessfully; 
	}
	
	public boolean delete(DataModel<T>[] dataModels) {
		this.statsModel.increaseRequests();
		Long[] ids = new Long[dataModels.length];
		boolean deletedSuccessfully = true;
		for (int i = 0; i < ids.length; i++) {
			DataModel<T> model = dataModels[i];
			this.statsModel.increaseTotalDataModelsRequests();
			if(model == null)
				continue;
			ids[i] = model.getDataModelId();
			try {
				this.dao.delete(model);
			} catch (IllegalArgumentException | IOException | ClassNotFoundException e) {
				e.printStackTrace();
				deletedSuccessfully = false;
				
			}
		}
		this.cacheUnit.removeDataModels(ids);
		return deletedSuccessfully; 
	}
	
	public DataModel<T>[] get(DataModel<T>[] dataModels){
		this.statsModel.increaseRequests();
		Long[] ids = new Long[dataModels.length];
		DataModel<T>[] result = new DataModel[dataModels.length];
		
		for (int i = 0; i < ids.length; i++) {
			this.statsModel.increaseTotalDataModelsRequests();
			DataModel<T> model = dataModels[i];
			if(model == null)
				continue;
			ids[i] = model.getDataModelId();
		}
		
		result = this.cacheUnit.getDataModels(ids);
		updateNotFoundInCache(result, ids);
		
		return result;
	}
	
	public StatsModel getStats() {
		return statsModel;
	}
	
	private void updateNotFoundInCache(DataModel<T>[] dataModels, Long[] ids) {
		for (int i = 0; i < dataModels.length; i++) { 
			DataModel<T> model = dataModels[i];
			if(model == null){
				try {
					dataModels[i] = this.dao.find(ids[i]);
				} catch (ClassNotFoundException | IOException e) {
					e.printStackTrace();
					dataModels[i] = null;
				}
			}
		}
	}
	
}
