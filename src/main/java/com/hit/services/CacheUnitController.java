package com.hit.services;
import com.hit.dm.DataModel;
import com.hit.dm.StatsModel;

public class CacheUnitController<T> {
	private CacheUnitService<T> cacheUnitService;

	public CacheUnitController() {
		this.cacheUnitService = new CacheUnitService<T>();
	}
	
	public boolean update(DataModel<T>[] dataModels) {
		return cacheUnitService.update(dataModels);
	}
	
	public boolean delete(DataModel<T>[] dataModels) {
		return cacheUnitService.delete(dataModels);
	}
	
	public DataModel<T>[] get(DataModel<T>[] dataModels){
		return cacheUnitService.get(dataModels);
	}

	public StatsModel getStats() {
		return cacheUnitService.getStats();
	}
}
