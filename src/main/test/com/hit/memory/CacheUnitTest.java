package com.hit.memory;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import com.hit.algorithm.IAlgoCache;
import com.hit.algorithm.NRUAlgoCacheImpl;
import com.hit.dao.DaoFileImpl;
import com.hit.dm.DataModel;


public class CacheUnitTest {

	@SuppressWarnings("unchecked")
	@Test
	public void getDataModelsTest() throws IOException, ClassNotFoundException {
		
		DataModel<String>[] allData = new DataModel[]{
			 new DataModel<String>(new Long(1), "Adam"),
			 new DataModel<String>(new Long(2), "Omri"),
			 new DataModel<String>(new Long(3), "Nissim")
		};
		
		
		DaoFileImpl<String> dao = new DaoFileImpl<String>(new File("src/main/resources/datasource.txt").getAbsolutePath());
		IAlgoCache<Long, DataModel<String>> algo = new NRUAlgoCacheImpl<Long, DataModel<String>>(2);
		CacheUnit<String> cache = new CacheUnit<String>(algo);
		
		
		DataModel<String>[] actualNotExists = cache.getDataModels((Long[])Arrays.asList(allData[0].getDataModelId()).toArray());
		Assert.assertEquals(null, actualNotExists[0]);
		
		DataModel<String>[] modelsToSave = (DataModel<String>[]) Arrays.asList(allData[0], allData[1]).toArray();
		DataModel<String>[] actualFreeCapacity = cache.putDataModels(modelsToSave);
		Assert.assertEquals(null, actualFreeCapacity[0]);
		Assert.assertEquals(null, actualFreeCapacity[1]);
		
		DataModel<String>[] actualExists = cache.getDataModels((Long[])Arrays.asList(allData[0].getDataModelId()).toArray());
		Assert.assertEquals("Adam", actualExists[0].getContent());
		
		DataModel<String>[] saveNoCapacity = (DataModel<String>[]) Arrays.asList(allData[2]).toArray();
		DataModel<String>[] actualNoCapacity = cache.putDataModels(saveNoCapacity);
		Assert.assertTrue(!actualNoCapacity[0].getContent().isEmpty());
		
		for (DataModel<String> dataModel : actualNoCapacity) {
			DataModel<String> found = dao.find(dataModel.getDataModelId());
			if(found != null) {
				dao.delete(found); //update file with data
			}
			dao.save(dataModel); // save data that is not in cache.
		}
		
		DataModel<String>[] fromExistingCache = cache.getDataModels((Long[])Arrays.asList(allData[0].getDataModelId(), allData[1].getDataModelId(), allData[2].getDataModelId()).toArray());
		for (DataModel<String> dataModel : fromExistingCache) {
			if(dataModel == null) {
				// not in cache.
				continue;
			}
			dao.save(dataModel); // save all cache data before shutdown
		}
		
		for (DataModel<String> dataModel : allData) {
			Assert.assertNotNull(dao.find(dataModel.getDataModelId()));
		}
		
	}
}
